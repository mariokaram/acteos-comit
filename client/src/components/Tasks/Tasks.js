import React from "react";
import { Table } from "reactstrap";
import EditIcon from "@material-ui/icons/Edit";

import RemoveCircleIcon from "@material-ui/icons/RemoveCircle";
const TableCustom = props => {
  return (
    <Table striped>
      <thead>
        <tr>
          <th>Description</th>
          <th>Amount</th>
          <th>Date</th>
          <th>Total : 260$</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">Christmass Event</th>
          <td>200$</td>
          <td>1/2/2010</td>
          <td>
            <EditIcon style={{ color: "green", cursor: "pointer" }} />
            <span style={{ paddingRight: "9px" }}></span>
            <RemoveCircleIcon style={{ color: "red", cursor: "pointer" }} />
          </td>
        </tr>
        <tr>
          <th scope="row">Popcorn Event</th>
          <td>50$</td>
          <td>2/3/2011</td>
          <td>
            <EditIcon style={{ color: "green", cursor: "pointer" }} />
            <span style={{ paddingRight: "9px" }}></span>
            <RemoveCircleIcon style={{ color: "red", cursor: "pointer" }} />
          </td>
        </tr>
        <tr>
          <th scope="row">Amchit Camping</th>
          <td>10$</td>
          <td>5/5/2017</td>
          <td>
            <EditIcon style={{ color: "green", cursor: "pointer" }} />
            <span style={{ paddingRight: "9px" }}></span>
            <RemoveCircleIcon style={{ color: "red", cursor: "pointer" }} />
          </td>
        </tr>
      </tbody>
    </Table>
  );
};

export default TableCustom;
