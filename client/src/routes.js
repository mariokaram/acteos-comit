/*!

=========================================================
* Material Dashboard React - v1.8.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
import Events from "@material-ui/icons/Event";
import AccountBalanceIcon from "@material-ui/icons/AccountBalance";
import Person from "@material-ui/icons/Person";

import BubbleChart from "@material-ui/icons/BubbleChart";
import LocationOn from "@material-ui/icons/LocationOn";
import Notifications from "@material-ui/icons/Notifications";
import DashboardPage from "views/Dashboard/Dashboard.js";
import EventsPage from "views/Events";
import UserProfile from "views/UserProfile/UserProfile.js";
import TableList from "views/TableList/TableList.js";
import Typography from "views/Typography/Typography.js";
import Icons from "views/Icons/Icons.js";
import Maps from "views/Maps/Maps.js";
import NotificationsPage from "views/Notifications/Notifications.js";
import AttachMoneyIcon from "@material-ui/icons/AttachMoney";
import CakeIcon from "@material-ui/icons/Cake";
import PaymentIcon from "@material-ui/icons/Payment";
import PlaylistAddCheckIcon from "@material-ui/icons/PlaylistAddCheck";
const dashboardRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: Dashboard,
    component: DashboardPage,
    layout: "/acteos"
  },

  {
    path: "/events",
    name: "Events",
    icon: Events,
    component: EventsPage,
    layout: "/acteos"
  },
  {
    path: "/icons",
    name: "Birthdays",
    icon: CakeIcon,
    component: Icons,
    layout: "/acteos"
  },
  {
    path: "/maps",
    name: "Payments",
    icon: PaymentIcon,
    component: Maps,
    layout: "/acteos"
  },
  {
    path: "/notifications",
    name: "Requests",
    icon: PlaylistAddCheckIcon,
    component: NotificationsPage,
    layout: "/acteos"
  }
];

export default dashboardRoutes;
