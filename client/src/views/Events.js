import React, { Component } from "react";
import AddCircleIcon from "@material-ui/icons/AddCircle";
// @material-ui/core
import { withStyles } from "@material-ui/core/styles";

import DateRange from "@material-ui/icons/DateRange";

import Person from "@material-ui/icons/Person";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import AccountBalanceIcon from "@material-ui/icons/AccountBalance";
import BugReport from "@material-ui/icons/BugReport";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";

import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardIcon from "components/Card/CardIcon.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import AttachMoneyIcon from "@material-ui/icons/AttachMoney";

import { bugs, website, server } from "variables/general.js";

import {
  dailySalesChart,
  emailsSubscriptionChart,
  completedTasksChart
} from "variables/charts.js";

import styles from "assets/jss/material-dashboard-react/views/dashboardStyle.js";
import { Table } from "@material-ui/core";
import Tasks from "components/Tasks/Tasks";
import dashboardStyle from "assets/jss/material-dashboard-react/views/dashboardStyle";

class Events extends Component {
  state = {
    modal: false,
    modalI: false
  };
  toggle = v => {
    if (v === "income") {
      this.setState({ modalI: !this.state.modalI });
    } else {
      this.setState({ modal: !this.state.modal });
    }
  };
  render() {
    const { classes } = this.props;

    return (
      <div>
        <GridContainer>
          <GridItem xs={12} sm={6} md={4}>
            <Card style={{}}>
              <CardHeader>
                <h4 style={{ textAlign: "center" }}>Camping</h4>
                <p style={{ fontWeight: "700" }}>Date </p> <p>2/2/2110</p>
                <p style={{ fontWeight: "700" }}>Location </p> <p>Sannine</p>
                <p style={{ fontWeight: "700" }}>Time</p>
                <p>9 am till 5 pm</p>
                <p style={{ fontWeight: "700" }}>Description</p>
                <p>we meet at the office then we ... </p>
              </CardHeader>
              <CardFooter>
                <Button
                  color="success"
                  onClick={() => this.setState({ modal: false })}
                >
                  Going (2){" "}
                </Button>{" "}
                <div>
                  {" "}
                  <Button
                    color="danger"
                    onClick={() => this.setState({ modal: false })}
                  >
                    Can't go (5)
                  </Button>{" "}
                </div>
              </CardFooter>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6} md={4}>
            <Card style={{}}>
              <CardHeader>
                <h4 style={{ textAlign: "center" }}>Hiking</h4>
                <p style={{ fontWeight: "700" }}>Date </p> <p>2/2/2110</p>
                <p style={{ fontWeight: "700" }}>Location </p> <p>Sannine</p>
                <p style={{ fontWeight: "700" }}>Time</p>
                <p>9 am till 5 pm</p>
                <p style={{ fontWeight: "700" }}>Description</p>
                <p>we meet at the office then we ... </p>
              </CardHeader>
              <CardFooter>
                <Button
                  color="success"
                  onClick={() => this.setState({ modal: false })}
                >
                  Going (2){" "}
                </Button>{" "}
                <div>
                  {" "}
                  <Button
                    color="danger"
                    onClick={() => this.setState({ modal: false })}
                  >
                    Can't go (5)
                  </Button>{" "}
                </div>
              </CardFooter>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6} md={4}>
            <Card style={{}}>
              <CardHeader>
                <h4 style={{ textAlign: "center" }}>Party</h4>
                <p style={{ fontWeight: "700" }}>Date </p> <p>2/2/2110</p>
                <p style={{ fontWeight: "700" }}>Location </p> <p>Sannine</p>
                <p style={{ fontWeight: "700" }}>Time</p>
                <p>9 am till 5 pm</p>
                <p style={{ fontWeight: "700" }}>Description</p>
                <p>we meet at the office then we ... </p>
              </CardHeader>
              <CardFooter>
                <Button
                  color="success"
                  onClick={() => this.setState({ modal: false })}
                >
                  Going (2){" "}
                </Button>{" "}
                <div>
                  {" "}
                  <Button
                    color="danger"
                    onClick={() => this.setState({ modal: false })}
                  >
                    Can't go (5)
                  </Button>{" "}
                </div>
              </CardFooter>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6} md={4}>
            <Card style={{}}>
              <CardHeader>
                <h4 style={{ textAlign: "center" }}>Bike</h4>
                <p style={{ fontWeight: "700" }}>Date </p> <p>2/2/2110</p>
                <p style={{ fontWeight: "700" }}>Location </p> <p>Sannine</p>
                <p style={{ fontWeight: "700" }}>Time</p>
                <p>9 am till 5 pm</p>
                <p style={{ fontWeight: "700" }}>Description</p>
                <p>we meet at the office then we ... </p>
              </CardHeader>
              <CardFooter>
                <Button
                  color="success"
                  onClick={() => this.setState({ modal: false })}
                >
                  Going (2)
                </Button>{" "}
                <div>
                  {" "}
                  <Button
                    color="danger"
                    onClick={() => this.setState({ modal: false })}
                  >
                    Can't go (5)
                  </Button>{" "}
                </div>
              </CardFooter>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6} md={4}>
            <Card style={{}}>
              <CardHeader>
                <h4 style={{ textAlign: "center" }}>Camping</h4>
                <p style={{ fontWeight: "700" }}>Date </p> <p>2/2/2110</p>
                <p style={{ fontWeight: "700" }}>Location </p> <p>Sannine</p>
                <p style={{ fontWeight: "700" }}>Time</p>
                <p>9 am till 5 pm</p>
                <p style={{ fontWeight: "700" }}>Description</p>
                <p>we meet at the office then we ... </p>
              </CardHeader>
              <CardFooter>
                <Button
                  color="success"
                  onClick={() => this.setState({ modal: false })}
                >
                  Going (2){" "}
                </Button>{" "}
                <div>
                  {" "}
                  <Button
                    color="danger"
                    onClick={() => this.setState({ modal: false })}
                  >
                    Can't go (5)
                  </Button>{" "}
                </div>
              </CardFooter>
            </Card>
          </GridItem>
        </GridContainer>

        <div>
          <Modal
            isOpen={this.state.modal}
            toggle={() => this.setState({ modal: false })}
          >
            <ModalHeader toggle={() => this.setState({ modal: false })}>
              Add Expenses
            </ModalHeader>
            <ModalBody>
              <div className="row" style={{ textAlign: "center" }}>
                <div className="col-md-6">
                  <textarea
                    className="form-control"
                    placeholder="Description..."
                  />
                </div>

                <div className="col-md-6">
                  <input className="form-control" type="date" />
                  <div style={{ paddingTop: "3px" }}>
                    <input
                      className="form-control"
                      type="number"
                      placeholder="Amount in $"
                    />
                  </div>
                </div>
              </div>
            </ModalBody>
            <ModalFooter>
              <Button
                color="success"
                onClick={() => this.setState({ modal: false })}
              >
                Add
              </Button>{" "}
              <Button
                color="secondary"
                onClick={() => this.setState({ modal: false })}
              >
                Cancel
              </Button>
            </ModalFooter>
          </Modal>
        </div>
        <div>
          <Modal
            isOpen={this.state.modalI}
            toggle={() => this.setState({ modalI: false })}
          >
            <ModalHeader toggle={() => this.setState({ modalI: false })}>
              Add Income
            </ModalHeader>
            <ModalBody>
              <div style={{ paddingTop: "3px" }}>
                <input
                  className="form-control"
                  type="number"
                  placeholder="Amount in $"
                />
              </div>
            </ModalBody>
            <ModalFooter>
              <Button
                color="success"
                onClick={() => this.setState({ modalI: false })}
              >
                Add
              </Button>{" "}
              <Button
                color="secondary"
                onClick={() => this.setState({ modalI: false })}
              >
                Cancel
              </Button>
            </ModalFooter>
          </Modal>
        </div>
      </div>
    );
  }
}
export default withStyles(dashboardStyle)(Events);
