import React, { Component } from "react";
import AddCircleIcon from "@material-ui/icons/AddCircle";
// @material-ui/core
import { withStyles } from "@material-ui/core/styles";

import DateRange from "@material-ui/icons/DateRange";

import Person from "@material-ui/icons/Person";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import AccountBalanceIcon from "@material-ui/icons/AccountBalance";
import BugReport from "@material-ui/icons/BugReport";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";

import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardIcon from "components/Card/CardIcon.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import AttachMoneyIcon from "@material-ui/icons/AttachMoney";

import { bugs, website, server } from "variables/general.js";

import {
  dailySalesChart,
  emailsSubscriptionChart,
  completedTasksChart
} from "variables/charts.js";

import styles from "assets/jss/material-dashboard-react/views/dashboardStyle.js";
import { Table } from "@material-ui/core";
import Tasks from "components/Tasks/Tasks";
import dashboardStyle from "assets/jss/material-dashboard-react/views/dashboardStyle";

class Dashboard extends Component {
  state = {
    modal: false,
    modalI: false
  };
  toggle = v => {
    if (v === "income") {
      this.setState({ modalI: !this.state.modalI });
    } else {
      this.setState({ modal: !this.state.modal });
    }
  };
  render() {
    const { classes } = this.props;

    return (
      <div>
        <GridContainer>
          <GridItem xs={12} sm={6} md={4}>
            <Card
              onClick={() => this.toggle("income")}
              style={{ cursor: "pointer" }}
            >
              <CardHeader color="success" stats icon>
                <CardIcon color="success">
                  <AttachMoneyIcon />
                </CardIcon>
                <p className={classes.cardCategory}>Income</p>
                <h3 className={classes.cardTitle}>
                  400 <small>$</small>
                </h3>
              </CardHeader>
              <CardFooter stats>
                <div className={classes.stats}>
                  <DateRange />
                  {new Date().toLocaleDateString()}
                </div>
              </CardFooter>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6} md={4}>
            <Card onClick={this.toggle} style={{ cursor: "pointer" }}>
              <CardHeader color="warning" stats icon>
                <CardIcon color="warning">
                  <ShoppingCartIcon />
                </CardIcon>
                <p className={classes.cardCategory}>Expenses</p>
                <h3 className={classes.cardTitle}>
                  260<small>$</small>
                </h3>
              </CardHeader>
              <CardFooter stats>
                <div className={classes.stats}>
                  <DateRange />
                  {new Date().toLocaleDateString()}
                </div>
              </CardFooter>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6} md={4}>
            <Card>
              <CardHeader color="danger" stats icon>
                <CardIcon color="danger">
                  <AccountBalanceIcon />
                </CardIcon>
                <p className={classes.cardCategory}>Balance</p>
                <h3 className={classes.cardTitle}>
                  240 <small>$</small>
                </h3>
              </CardHeader>
              <CardFooter stats>
                <div className={classes.stats}>
                  <DateRange />
                  {new Date().toLocaleDateString()}
                </div>
              </CardFooter>
            </Card>
          </GridItem>
        </GridContainer>
        <div>
          <hr></hr>
          <div className="row">
            <h6 className="col-md-8" style={{ color: " #ffa726" }}>
              Expenses
            </h6>
            <div className="col-md-4">
              <span style={{ paddingRight: "3px" }}>Filter by date</span>
              <input type="date" placeholder="search" />
            </div>
          </div>
          <Tasks />
        </div>
        <div>
          <Modal
            isOpen={this.state.modal}
            toggle={() => this.setState({ modal: false })}
          >
            <ModalHeader toggle={() => this.setState({ modal: false })}>
              Add Expenses
            </ModalHeader>
            <ModalBody>
              <div className="row" style={{ textAlign: "center" }}>
                <div className="col-md-6">
                  <textarea
                    className="form-control"
                    placeholder="Description..."
                  />
                </div>

                <div className="col-md-6">
                  <input className="form-control" type="date" />
                  <div style={{ paddingTop: "3px" }}>
                    <input
                      className="form-control"
                      type="number"
                      placeholder="Amount in $"
                    />
                  </div>
                </div>
              </div>
            </ModalBody>
            <ModalFooter>
              <Button
                color="success"
                onClick={() => this.setState({ modal: false })}
              >
                Add
              </Button>{" "}
              <Button
                color="secondary"
                onClick={() => this.setState({ modal: false })}
              >
                Cancel
              </Button>
            </ModalFooter>
          </Modal>
        </div>
        <div>
          <Modal
            isOpen={this.state.modalI}
            toggle={() => this.setState({ modalI: false })}
          >
            <ModalHeader toggle={() => this.setState({ modalI: false })}>
              Add Income
            </ModalHeader>
            <ModalBody>
              <div style={{ paddingTop: "3px" }}>
                <input
                  className="form-control"
                  type="number"
                  placeholder="Amount in $"
                />
              </div>
            </ModalBody>
            <ModalFooter>
              <Button
                color="success"
                onClick={() => this.setState({ modalI: false })}
              >
                Add
              </Button>{" "}
              <Button
                color="secondary"
                onClick={() => this.setState({ modalI: false })}
              >
                Cancel
              </Button>
            </ModalFooter>
          </Modal>
        </div>
      </div>
    );
  }
}
export default withStyles(dashboardStyle)(Dashboard);
