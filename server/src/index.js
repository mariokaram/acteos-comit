// import app from "./app";
// import initializeDatabase from "./db";

// const start = async () => {
//   const controller = await initializeDatabase();

//   // READ
//   app.get("/n", async (req, res, next) => {
//     try {
//       // const id = req.params.id;
//       // const images = req.query.images;

//       // console.log(id);
//       // console.log({ images });

//       const contact = await controller.batchingThings();
//       res.json({ success: true, result: contact });
//     } catch (e) {
//       next(e);
//     }
//   });

//   app.get("/up", async (req, res, next) => {
//     try {
//       // const id = req.params.id;
//       // const images = req.query.images;

//       // console.log(id);
//       // console.log({ images });

//       const contact = await controller.upload();
//       res.json({ success: true, result: contact });
//     } catch (e) {
//       next(e);
//     }
//   });

//   // ERROR
//   app.use((err, req, res, next) => {
//     console.error(err);
//     const message = err.message;
//     res.status(500).json({ success: false, message });
//   });

//   app.listen(8080, () => console.log("server listening on port 8080"));
// };
// start();
