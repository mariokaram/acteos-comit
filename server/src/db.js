// const mysql = require("mysql2/promise");
// import SQL from "sql-template-strings";
// import { map } from "lodash";

// const initializeDatabase = async () => {
//   var pool = mysql.createPool({
//     connectionLimit: 100, //important
//     host: "localhost",
//     user: "root",
//     password: "password",
//     database: "decorzain",
//     debug: false
//   });
//   pool.getConnection((err, connection) => {
//     if (err) {
//       if (err.code === "PROTOCOL_CONNECTION_LOST") {
//         console.error("Database connection was closed.");
//       }
//       if (err.code === "ER_CON_COUNT_ERROR") {
//         console.error("Database has too many connections.");
//       }
//       if (err.code === "ECONNREFUSED") {
//         console.error("Database connection was refused.");
//       }
//     }
//     if (connection) connection.release();
//     return;
//   });

//   async function upload() {
//     const connection = await pool.getConnection();
//     await connection.beginTransaction();
//     try {
//       await connection.query(
//         SQL`delete  FROM decorzain.tbl_users_images_members where member_id = 34`
//       );

//       await connection.query(
//         SQL`insert into tbl_users_images_members (member_id , images_id, images) values ( '34' , 'living_room'
//          ,'nour.png') ,('34' , 'living_room' , 'khara.png' ), ( '34', 'salon_room' , 'jahech.png')
//         `
//       );

//       await connection.commit();
//     } catch (err) {
//       await connection.rollback();
//       // Throw the error again so others can catch it.
//       throw err;
//     } finally {
//       connection.release();
//     }
//   }

//   async function batchingThings() {
//     const connection = await pool.getConnection();
//     await connection.beginTransaction();

//     try {
//       const result = await connection.query(
//         SQL`INSERT INTO tbl_users_members (name, country) VALUES('tony', 'syria');`
//       );

//       var log1 = result[0].insertId;

//       let arrayData = [
//         {
//           living_room: "1",
//           bed_room: "0",
//           kitchen: "0",
//           salon: "4",
//           diner_room: "0",
//           plan: "silver"
//         },
//         {
//           living_room: "0",
//           bed_room: "3",
//           kitchen: "0",
//           salon: "0",
//           diner_room: "1",
//           plan: "golden"
//         }
//       ];

//       await connection.query(SQL`INSERT INTO tbl_users_rooms (living_room, bed_room,kitchen,salon,diner_room,plan,member_id)
//       values ('1','0','0','0','0','silver',${log1}),('1','0','0','0','0','golden',${log1})
//      `);

//       await connection.commit();
//     } catch (err) {
//       await connection.rollback();
//       // Throw the error again so others can catch it.
//       throw err;
//     } finally {
//       connection.release();
//     }
//   }

//   const controller = {
//     batchingThings,
//     upload
//   };

//   return controller;
// };

// export default initializeDatabase;
